<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet"/>
    <link href="{{url('/css/app.css')}}" rel="stylesheet"/>

</head>
<body class="antialiased">
<div id="container">
    <aside>
        <header>
            <input type="text" placeholder="search">
        </header>
        <ul>
            @foreach($users as $user_list)
            <li>
                <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1940306/chat_avatar_01.jpg" alt="">
                <div>
                    <h2>{{$user_list->name}}</h2>
                    <h3>
                        <span class="status orange"></span>
                        {{$user_list->last_messages}}
                    </h3>
                </div>
            </li>
            @endforeach
        </ul>
    </aside>
    <main>
        <header>
            <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1940306/chat_avatar_01.jpg" alt="">
            <div>
                <h2>Chat with {{$friend->name ?? ''}}</h2>
                <h3>Already {{count($messages)}} messages</h3>
            </div>
            <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1940306/ico_star.png" alt="">
        </header>
        <ul id="chat">
            @foreach($messages as $message)
            <li class="{{$message->user_id_send === $user->id ? 'me':'you'}}">
                <div class="entete">
                    <span class="status green"></span>
                    <h2>{{$message->user_id_send === $user->id ? $user->name:$friend->name}}</h2>
                    <h3>{{$message->created_at}}</h3>
                </div>
                <div class="triangle"></div>
                <div class="message">
                    {{$message->message}}
                </div>
            </li>
            @endforeach
        </ul>
        <footer>
            <textarea placeholder="Type your message"></textarea>
            <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1940306/ico_picture.png" alt="">
            <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1940306/ico_file.png" alt="">
            <a href="#">Send</a>
        </footer>
    </main>
</div>
</body>
</html>

import './bootstrap';

import {createApp, h} from 'vue'

import App from '../vue/App.vue'
import {router} from "../vue/router";

createApp({
    render: () => h(App)
}).use(router).mount("#app")

<?php

namespace App\Http\Controllers;

use App\Events\Chat;
use App\Models\Friend;
use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Sanctum\PersonalAccessToken;

class ChatController extends Controller
{
    public function messages() {

    }

    public function list_user(Request $request) {
        $token = PersonalAccessToken::findToken($request->bearerToken());
        $userId = $token->tokenable->id;
        $userIdChat = $request->user_id ?? 0;
        $data = [
            "user" => $token->tokenable,
            "users" => [],
            "friend" => [],
            "messages" => [],
        ];
        $Idfriend = Friend::select(
            DB::raw("IF(user_id1 = $userId, user_id2, user_id1) as user_id")
        )
             ->where('user_id1', $userId)
            ->orWhere('user_id2', $userId)
            ->get()->pluck('user_id')->toArray();
        $userFriends = User::whereIn('id', $Idfriend)->get();
        $data = array_merge($data,[
            'users' => $userFriends,
            'friend' => [],
            'messages' => [],
        ]);
        if($userIdChat) {
            $friend = Friend::select(
                DB::raw("IF(user_id1 = $userId, user_id2, user_id1) as user_id"),
                'id'
            )->where('user_id1', $userId)->where('user_id2', $userIdChat)->orWhere(function ($query) use ($userId, $userIdChat){
                $query->where('user_id1', $userIdChat)->where('user_id2', $userId);
            })->first();
            if($friend) {
                $userFriend = $userFriends->filter(function ($query) use ($friend){
                    return $query->id === $friend?->user_id;
                })->first();
                $messages = Message::where('friend_id', $friend->user_id)->get();
                $data["friend"] = $userFriend;
                $data["messages"] = $messages;
            }
        }
        return view('welcome', $data);
    }
    public function send(Request $request) {
        $friend_id = $request->friend_id;
        $user_id = $request->user_id ?? $request->user()->id;
        $message = $request->message;
        $message_create = Message::create([
            "message" => $message,
            "user_id_send" => $user_id,
            "friend_id" => $friend_id
        ]);
        event(new Chat('hello world'));
        return response()->json([
            'data' => $message_create,
            'status' => 1,
            'message' => 'success',
        ]);
    }
}
